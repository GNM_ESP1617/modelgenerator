from time import gmtime, strftime
import errno
import os
from datetime import datetime
import subprocess


def createDir(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


showtime = strftime("%Y-%m-%d_%H:%M:%S", gmtime())
directory = "./Log_" + showtime

count = 0

directory = os.path.join(
    os.getcwd(),
    datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
training_dir = directory + "/trainingOutput"
evaluation_dir = directory + "/evaluationOutput"

createDir(directory)
createDir(training_dir)
createDir(evaluation_dir)


command_train = "(time python train_image_classifier.py \
                --train_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/Fine-TuningTFSLIM \
                --dataset_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/MIOdataset/dataset/xiaomiTFRecord \
                --dataset_split_name=train \
                --model_name=inception_v3 \
                --max_number_of_steps=500 \
                --checkpoint_path=models/inception_v3.ckpt \
                --checkpoint_exclude_scopes=InceptionV3/Logits,InceptionV3/AuxLogits \
                --trainable_scopes=InceptionV3/Logits,InceptionV3/AuxLogits) &>" + training_dir + "/output.txt"

command_eval = "(time python evaluationPerformanceRetrainedModel.py " \
          "--checkpoint_path=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/Fine-TuningTFSLIM/model.ckpt-3 " \
          "--dataset_split_name=validation --eval_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/Fine-TuningTFSLIM/logEvaluation " \
          "--dataset_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/MIOdataset/dataset/xiaomiTFRecord --model_name=inception_v3) &>" + evaluation_dir + "/output.txt"


command_train = "(time python train_image_classifier.py \
                --train_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/maggio2017\
                --dataset_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/MIOdataset/dataset/xiaomiTFRecord \
                --dataset_split_name=train \
                --model_name=inception_resnet_v2 \
                --max_number_of_steps=3 \
                --checkpoint_path=models/inception_resnet_v2_2016_08_30.ckpt \
                --checkpoint_exclude_scopes=InceptionResnetV2/Logits,InceptionResnetV2/AuxLogits \
                --trainable_scopes=InceptionResnetV2/Logits,InceptionResnetV2/AuxLogits)"

command_eval = "(time python evaluationPerformanceRetrainedModel.py " \
          "--checkpoint_path=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/Fine-TuningTFSLIM/model.ckpt-3 " \
          "--dataset_split_name=validation " \
               "--eval_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/Fine-TuningTFSLIM/logEvaluation " \
          "--dataset_dir=/Users/marcobrugnera/Desktop/Universita/embedded/PROGETTO/MIOdataset/dataset/xiaomiTFRecord " \
               "--model_name=inception_v3) &>" + evaluation_dir + "/output.txt"


os.system(command_train)
#os.system(command_eval)


#def getString():
#    directory = os.path.join(
#        os.getcwd(),
#        datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
#    return

