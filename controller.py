import os.path
from datetime import datetime
import errno
import controller
from os import rename



directory = ""
directory_train = ""
directory_evaluation = ""
directory_output_preprocessing = ""
data_and_time = ""
num_iteration = 2000  #Number of steps to set in the training command



"""
This function create a directory if it does not alredy exists.
"""
def createDir(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


"""
This function verify if all the needed directories required during the execution existed, otherwise it creates them.
"""
def setup(answare):
    if(answare == 1):
        controller.directory = os.getcwd() + "/proceduraTF/logs"
        controller.directory_output_preprocessing = os.getcwd() + "/proceduraTF/bottlenecks"
        controller.directory_out_evaluation = os.getcwd() + "/proceduraTF/outputEvaluation"
    else:
        controller.directory = os.getcwd() + "/proceduraTFSlim/logs"
        controller.directory_output_preprocessing = os.getcwd() + "/proceduraTFSlim/datasetTF"
        controller.directory_out_evaluation = os.getcwd() + "/proceduraTFSlim/outputEvaluation"
    createDir(controller.directory)
    createDir(controller.directory_output_preprocessing)
    #get the current data and time
    controller.data_and_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    controller.directory_train = os.path.join(
        controller.directory,
        controller.data_and_time)
    controller.directory_evaluation = os.path.join(
        controller.directory,
        controller.data_and_time)
    createDir(controller.directory_train)
    createDir(controller.directory_evaluation)
    controller.directory_train = controller.directory_train + "/train"
    controller.directory_evaluation = controller.directory_evaluation + "/evaluation"
    createDir(controller.directory_train)
    createDir(controller.directory_evaluation)
    createDir(controller.directory_out_evaluation)


print("During the research made to developed our Android app whe discovered that there are two different libraries: "
      "TensorFlow and TensorFlow Slim. \nWithin the tutorials released by Google there are two different procedures "
      "depending on the library used. \nThis program is designed to make available a tool that brings together all the "
      "steps necessary to make both the two procedures.\n")

print("Press: \n\t 1 to procede with the first procedure (that used TensorFlow Library)"
      "\n\t 2 to procede with the second procedure (that used TensorFlow Slim Library)")

answare = input(": ")
valid_answare = False
while(valid_answare == False):
    if(answare == 1 or answare == 2 ):
        valid_answare = True
    else:
        print("The pressed key do not correspond to a valid option. Please try again.")
        answare = input(": ")


print "Insert the path where is stored the dataset that is wanted to used during the fine-tuning operation.\n" \
      "The directory have to contains one directory for each of the classes on which is desiderate to realized the fine_tuning operation." \

dataset_directory = raw_input()
valid_answare = False
while(valid_answare == False):
    empty_directory = False
    only_directory = True
    if os.path.exists(dataset_directory):
        subdirectories = os.listdir(dataset_directory)
        if len(subdirectories) == 1:
            empty_directory = True;
            print "The inserted path correspond to an empty directory. Verify that the path is correct or insert a new one."
        for dir in subdirectories:
            filename, file_extension = os.path.splitext(dir)
            if os.path.isdir(dir) == False and file_extension != "":
                print "The inserted path correspond to a directory that do not cointains only directories. Insert a new path or remove the files from the directory."
                only_directory = False
        if(empty_directory == False and only_directory == True):
            valid_answare = True
        else:
            dataset_directory = raw_input(": ")
    else:
        print "The inserted path do not correspond to a valid directory. Please try again."
        dataset_directory = raw_input(": ")


setup(answare)

if(answare == 1):
    #command for the procedure 1 (TensorFlow)
    command_train = "(time python ./proceduraTF/retrain.py \
               --bottleneck_dir=" + directory_output_preprocessing + "\
               --how_many_training_steps=" + str(num_iteration) + "\
               --model_dir=./proceduraTF/inception \
               --output_graph=./proceduraTF/outputModel/retrain_" + str(num_iteration) + "_" + data_and_time + ".pb \
               --output_labels=./proceduraTF/outputModel/labelRetrain_" + str(num_iteration) + "_" + data_and_time + ".txt \
               --image_dir=" + dataset_directory + ") &>" + directory_train + "/output.txt"

    os.system(command_train)

if(answare == 2):
    print "It is possible to choose between different CNN to use has as the starting base to perform the fine-tuning operation." \
          "\n\t 1 to select the inception v1 model" \
          "\n\t 2 to select the inception v3 model" \
          "\n\t 3 to select the inception v4 model"

    selected_model = input(": ")
    valid_answare = False
    while(valid_answare == False):
        if(selected_model < 1 or selected_model > 5):
            print "The selected option is not valide. Please pres the key that corrispond to a digit into the range [1,5]."
            selected_model = input(": ")
        else:
            valid_answare = True

    model_name = ""
    label_model_output = ""
    if selected_model == 1:
        model_name = "inception_v1"
        label_model_output = "InceptionV1"
    else:
        if selected_model == 2:
            model_name = "inception_v3"
            label_model_output = "InceptionV3"
        else:
            if selected_model == 3:
                model_name = "inception_v4"
                label_model_output = "InceptionV4"

    #create the TFRecords
    dir_empty = True
    files = os.listdir(directory_output_preprocessing)
    for file in files:
        filename, file_extension = os.path.splitext(file)
        if file_extension != "" and file_extension == ".tfrecord":
            dir_empty = False

    if dir_empty == True:
        comand_tfrecord = "(python ./proceduraTFSlim/create_tfrecord.py " \
                      "--dataset_dir=" + dataset_directory + \
                      " --tfrecord_filename=" + directory_output_preprocessing +"/ )"

        os.system(comand_tfrecord)

        #adjusting the name of the files created by the execution of comand_tfrecord
        files = os.listdir(directory_output_preprocessing)
        for file in files:
            filename, file_extension = os.path.splitext(file)
            if file_extension != "":
                if file[:1] == "_":
                    p = file[2]
                    if file[1] == "t":
                        rename(directory_output_preprocessing + "/" + file,
                               directory_output_preprocessing + "/train-" + file[7:])
                    else:
                        rename(directory_output_preprocessing + "/" + file, directory_output_preprocessing + "/validation-" + file[12:])

    #command for the procedure 2 (TFSlim)
    command_train = "(time python ./proceduraTFSlim/train_image_classifier.py \
                    --train_dir=./proceduraTFSlim/outputModel\
                    --dataset_dir=./proceduraTFSlim/datasetTF\
                    --dataset_split_name=train \
                    --model_name=" + model_name + "\
                    --max_number_of_steps=" + str(num_iteration) + " \
                    --checkpoint_path=./proceduraTFSlim/models/" + model_name + ".ckpt \
                    --checkpoint_exclude_scopes=" + label_model_output + "/Logits," + label_model_output + "/AuxLogits \
                    --trainable_scopes=" + label_model_output + "/Logits," + label_model_output + "/AuxLogits) &>" + directory_train + "/output.txt"

    command_eval = "(time python ./proceduraTFSlim/evaluationPerformanceRetrainedModel.py " \
                   "--checkpoint_path=./proceduraTFSlim/outputModel/model.ckpt-" + str(num_iteration)+ " " + \
                   "--dataset_split_name=validation " \
                   "--eval_dir=./proceduraTFSlim/outputEvaluation " \
                   "--dataset_dir=./proceduraTFSlim/datasetTF " \
                   "--model_name=" + model_name +") &>" + directory_evaluation + "/output.txt"


    #execute the training and the evaluation of the obtained re-trained model
    os.system(command_train)
    os.system(command_eval)

exit(0)
