This repository contain Python code that we have used to retrain a model with the libraries TensorFlow and TensorFlow Slim.

To execute the model generator, start a new terminal and move to the directory where is the controller.py script. Then run it with the command: **python controller.py**

Otherwise open it as project with a Python IDE (e.g PyCharm) and set as the running script: controller.py